const colors = require('tailwindcss/colors');

/** @typedef {import('tailwindcss/defaultTheme')} defaultTheme */
const defaultTheme = require('tailwindcss/defaultTheme');

/** @type {import('tailwindcss').Config} */
module.exports = {
    darkMode: 'class',
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        fontFamily: {
            body: ['Cabin', ...defaultTheme.fontFamily.sans],
            display: ['Raleway', ...defaultTheme.fontFamily.sans],
            DEFAULT: this.body,
        },
        extend: {

        },
    },
    plugins: [
        require("@tailwindcss/aspect-ratio"),
        require("@tailwindcss/typography"),
        require("@tailwindcss/forms")
    ],
}
