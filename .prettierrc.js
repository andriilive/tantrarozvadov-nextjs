module.exports = {
    "semi": false,
    "tabWidth": 4,
    "jsxSingleQuote": true,
    "bracketSpacing": true,
    "endOfLine": "lf",
    "useTabs": false,
    "htmlWhitespaceSensitivity": "ignore",
    "printWidth": 5000,
    "singleQuote": true,
    "trailingComma": "es5",
    "bracketSameLine": false,
    "plugins": [
        "prettier-plugin-tailwind"
    ]
}
