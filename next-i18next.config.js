const path = require('path');
/** @type {import('next-i18next').InternalConfig} */
module.exports = {
    debug: process.env.NODE_ENV === 'development',
    i18n: {
        locales: ['cz', 'en'],
        defaultLocale: 'cz',
        localePath: path.resolve('./public/locales'),
        localeDetection: process.env.NODE_ENV !== 'development',
    },
    fallbackLng: {
        default: ['cz']
    }
}
