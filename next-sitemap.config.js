/** @type {import('next-sitemap').IConfig} */
module.exports = {
    siteUrl: process.env.SITE_URL,
    exclude: ['/404'],
    generateIndexSitemap: false,
    priority: 1,
    changefreq: 'weekly',
    alternateRefs: [
        {
            href : `${process.env.SITE_URL}/en/`,
            hreflang : 'en',
            hrefIsAbsolute : true
        },
    ],
    additionalPaths: async (config) => {
        const result = [];

        const customRoutes = {
            markets : {
                de : ['test'],
                en : ['test'],
            },
        }

        for (const [key, value] of Object.entries(customRoutes)) {

            value.de.forEach((item, i) => {
                result.push({
                    loc: `${process.env.SITE_URL}/${key}/` + encodeURI(item),
                    priority: 0.7,
                    changefreq: 'weekly',
                    lastmod: new Date().toISOString(),

                    alternateRefs: [
                        {
                            href: `${process.env.SITE_URL}/en/${key}/${encodeURI(value.en[i])}`,
                            hreflang: 'en',
                            hrefIsAbsolute: true
                        },
                        {
                            href: `${process.env.SITE_URL}/${key}/${encodeURI(item)}`,
                            hreflang: 'de_AT',
                            hrefIsAbsolute: true
                        },
                    ],
                })
            });

        }

        return result
    },
    generateRobotsTxt: true
}
