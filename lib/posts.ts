import fs from 'fs'
import path from 'path'

const postsDirectory = path.join(process.cwd(), 'posts')

export function getSortedPostsData() {
    // Get file names under /posts
    const fileNames = fs.readdirSync(postsDirectory)
    const allPostsData = fileNames.map(fileName => {
        // Remove ".md" from file name to get id
        const id = fileName.replace(/\.mdx$/, '')

        // Read markdown file as string
        const fullPath = path.join(postsDirectory, fileName);
        const stats = fs.statSync(fullPath);

        const created = stats.birthtime.toString();
        const modified = stats.mtime.toString();

        // Combine the data with the id
        return {
            id,
            created,
            modified
        }
    })
    // Sort posts by date
    return allPostsData.sort((a, b) => {
        return -1
    })
}

export function getAllPostIds() {
    const fileNames = fs.readdirSync(postsDirectory)
    return fileNames.map(fileName => {
        return {
            params: {
                id: fileName.replace(/\.mdx$/, ''),
            },
        }
    })
}

export async function getPostData(id: string) {
    const fullPath = path.join(postsDirectory, `${id}.mdx`);
    const stats = fs.statSync(fullPath);
    const created = stats.birthtime.toString();
    const modified = stats.mtime.toString();

    // Combine the data with the id and contentHtml
    return {
        id,
        created,
        modified
    }
}
