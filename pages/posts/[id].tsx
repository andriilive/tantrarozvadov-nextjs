import Layout from '../../components/layout'
import { getAllPostIds, getPostData } from '../../lib/posts'
import Head from 'next/head'
import Date from '../../components/date'
import { GetStaticPaths, GetStaticProps } from 'next'

type PostProps = {
    postData: {
        id: string,
        created: string,
        modified: string
    }
}

export default function Post({ postData } : PostProps) {
    return (<Layout>
        <Head>
            <title>{postData.id}</title>
        </Head>
        <article>
            <h1>{postData.id}</h1>
            <div>
                <Date dateString={postData.created} />
            </div>
        </article>
    </Layout>)
}

export const getStaticPaths: GetStaticPaths = async () => {
    const paths = getAllPostIds()
    return {
        paths, fallback: false,
    }
}

export const getStaticProps: GetStaticProps = async ({ params }) => {
    const postData = await getPostData(params.id as string)
    return {
        props: {
            postData,
        },
    }
}
