import { NextSeo } from 'next-seo'
import Layout from '../components/layout'

export default function FourZeroFour() {
    return (
        <>
            <NextSeo title={'404'} />
            <Layout>
                <section className={'text-center'}>
                    404
                </section>
            </Layout>
        </>
    )
}
