import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import Link from 'next/link'
import Date from '../components/date'
import { GetStaticProps } from 'next'
import { getSortedPostsData } from '../lib/posts'

type HomeProps = {
    allPostsData: {
        modified: string,
        created: string,
        id: string
    }[]
}

export default function Home({ allPostsData }: HomeProps) {
    return (
        <Layout home>
            <Head>
                <title>{siteTitle}</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <section>
                <p>[Your Self Introduction]</p>
                <p>
                    (This is a sample website - you’ll be building a site like this in{' '}
                    <a href='https://nextjs.org/learn'>our Next.js tutorial</a>.)
                </p>
            </section>
            <section>
                <h2>Blog</h2>
                <ul>
                    {allPostsData.map(({ id, modified, created }, i) => (
                        <li key={id}>
                            <Link href={`/posts/${id}`}>
                                <a>{i}</a>
                            </Link>
                            <br />
                            <small>
                                <Date dateString={created} />
                            </small>
                        </li>
                    ))}
                </ul>
            </section>
        </Layout>
    )
}

export const getStaticProps: GetStaticProps = async () => {
    const allPostsData = getSortedPostsData()
    return {
        props: {
            allPostsData,
        },
    }
}
