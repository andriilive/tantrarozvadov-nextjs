const withMDX = require('next-mdx.config.js');
const i18n = require('next-i18next.config.js');
const sitemaps = require('next-sitemap.config');

/** @type {import('next').NextConfig} */
const nextConfig = {
    i18n,
}

module.exports = withMDX(nextConfig);
