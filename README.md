This is a starter template for [Learn Next.js](https://nextjs.org/learn).

## Git Multiple Repos Config: 
```
[core]
	repositoryformatversion = 0
	filemode = true
	bare = false
	logallrefupdates = true
	ignorecase = true
	precomposeunicode = true
[remote "origin"]
	url = git@github.com:sensualtouch/tantrarozvadov-nextjs.git
	url = git@gitlab.com:andriilive/tantrarozvadov-nextjs.git
	fetch = +refs/heads/*:refs/remotes/origin/*
[branch "main"]
	remote = origin
	merge = refs/heads/main

```
